// import { setupFiles } from "../jest.config";

declare module '*.css' {
    const styles: { [key: string]: string };
    export = styles;
}