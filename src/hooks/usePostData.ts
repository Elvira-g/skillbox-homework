import axios from 'axios';
import { useEffect, useState, useContext } from 'react';
import { tokenContext } from '../shared/context/tokenContext';

interface IPostData {
    data: {
        title?: string,
        author_fullname?: string,
        url?: string,
        id?: string, 
    }
    
}

export function usePostData () {
    const [data, setData] = useState<IPostData[]>([]);
    const token = useContext(tokenContext);

    useEffect(() => {
        axios.get('https://oauth.reddit.com/best', {
            headers: { Authorization: `bearer ${token}` }
        })
            // .then(console.log)
            .then((resp) => {
                const postData = resp.data.data.children;
                console.log(postData);
                setData(postData);
            })
            .catch(console.log);
    }, [token])
    
    return [data];
}