import React from 'react';
import './main.global.css';
import { Layout } from './shared/Layout';
import { Content } from './shared/Content';
import { Header } from './shared/Header';
import { CardsList } from './shared/CardsList';
import { Dropdown } from './shared/Dropdown';
import { hot } from 'react-hot-loader/root';
import { useToken } from './hooks/useToken';
import { tokenContext } from './shared/context/tokenContext';
import { UserContextProvider } from './shared/context/userContext';
import { postContext, PostContextProvider } from './shared/context/PostContext';

function AppComponent() {
    const [token] = useToken();

    return (
        <tokenContext.Provider value={token}>
            <UserContextProvider>
                <Layout>
                    <Header />
                    <Content> 
                        <PostContextProvider>
                          <CardsList />  
                        </PostContextProvider>
                        
                    </Content>
                    <br/>
                    <Dropdown button={<button>Test</button>}>
                            <CardsList />
                    </Dropdown>
                </Layout>
                
            </UserContextProvider>
                
        </tokenContext.Provider>

    )
}

// export const App = hot(AppComponent);
export const App = hot(() => <AppComponent/>);