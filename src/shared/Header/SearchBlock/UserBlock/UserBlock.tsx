import React from 'react';
import styles from './userblock.css';
import { AnanIcon } from '../../../icons';

interface IUserBlockProps {
  avatarSrc?: string;
  username?: string
}
 
export function UserBlock({ avatarSrc, username}: IUserBlockProps) {
  return (
    <a 
      href="https://www.reddit.com/api/v1/authorize?client_id=RANNPRCY_EwTR-sW20NK6w&response_type=code&state=random_string&redirect_uri=http://localhost:3000/auth&duration=permanent&scope=read submit identity"
      className={styles.userBox}
    >
      <div className={styles.avatarBox}>
        {avatarSrc
          ?<img src={avatarSrc} alt="user" className={styles.avatarImage}  />
          : <AnanIcon />
        }
      </div>

      <div className={styles.username}>
        <p>{username || 'Аноним'}</p>
      </div>
    </a>
  );
}
