import React from 'react';
import styles from './content.css';

export const Content: React.FC = ({ children }) => {
  return (
    <main className={styles.content}>
      {children}
    </main>
  );
}
 