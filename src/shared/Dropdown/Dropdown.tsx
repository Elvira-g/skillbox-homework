import React from 'react';
import styles from './dropdown.css';

interface IDropdownProps {
  button: React.ReactNode;
}

export const Dropdown: React.FC<IDropdownProps> = ({ button, children }) => {
  const [isDropdownOpen, setIsDropdownOpen] = React.useState(false);

  const handleToggleOpened = () => {
    setIsDropdownOpen(!isDropdownOpen);
  }

  return (
    <div className={styles.container}>
        <div onClick={handleToggleOpened}>
          { button }
        </div>
        {isDropdownOpen && (
          <div className={styles.listContainer}>
            <div className={styles.list} onClick={handleToggleOpened}>
              { children }
            </div>
          </div>
        )}
    </div>
  );
}
