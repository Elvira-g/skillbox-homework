import React from 'react';
import styles from './listitemdesktop.css';
import { BlockIcon, WarningIcon, CommentIcon, ShareIcon, SaveIcon } from '../../icons';

interface IListItemProps {
  postId: string;
}

export const ListItemDesktop: React.FC<IListItemProps> = ({postId, children}) => {
  return (
        <div className={styles.listItemDesctop}>
          {children}
          <div className={styles.listTextItems}>
            <button className={styles.menuItem}>
              <CommentIcon />
              <p className={styles.textButton}>Комментарии</p>
            </button>
            <button className={styles.menuItem}>
              <ShareIcon />
              <p className={styles.textButton}>Поделиться</p>
            </button>
            <button className={styles.menuItem} onClick={() => console.log(postId)}>
              <BlockIcon /> 
              <p className={styles.textButton}>Скрыть</p>
            </button>
            <button className={styles.menuItem}>
              <SaveIcon />
              <p className={styles.textButton}>Сохранить</p>
            </button>
            <button className={styles.menuItem}>
              <WarningIcon />
              <p className={styles.textButton}>Пожаловаться</p>
            </button>
          </div>
           <button className={styles.closeButton}>
            Закрыть
          </button>
        </div>
      );
}
