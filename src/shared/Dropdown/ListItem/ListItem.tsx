import React from 'react';
import styles from './listitem.css';
import { BlockIcon, WarningIcon } from '../../icons';

interface IListItemProps {
  postId: string;
  // categories: string;
}

export const ListItem: React.FC<IListItemProps> = ({postId, children}) => {
  return (
        <div className={styles.listItem}>
          {children}
          <div className={styles.listTextItems}>
            <button className={styles.menuItem} onClick={() => console.log(postId)}>
              <BlockIcon /> 
              <p className={styles.textButton}>Скрыть</p>
            </button>
            <button className={styles.menuItem}>
              <WarningIcon />
              <p className={styles.textButton}>Пожаловаться</p>
            </button>
          </div>
           <button className={styles.closeButton}>
            Закрыть
          </button>
        </div>
      );
}
