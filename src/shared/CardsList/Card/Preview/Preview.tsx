import React from 'react';
import styles from './preview.css';
import post1 from './img/post1.jpg';

export function Preview() {
  return (
    <div className={styles.preview}>
        <img 
          className={styles.previewImg}
          src={post1}
        />
      </div>
  );
}
