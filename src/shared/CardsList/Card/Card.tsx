import React from 'react';
import styles from './card.css';
import { TextContent } from './TextContent';
import { Preview } from './Preview';
import { Menu } from './Menu';
import { Controls } from './Controls';
import { Dropdown } from '../../Dropdown';
import { ListItem } from '../../Dropdown/ListItem';
import { CommentsButton } from './Controls/CommentsButton';
import { ListItemDesktop } from '../../Dropdown/ListItemDesktop';

interface ICardProps {
  title?: string,
  author_fullname?: string,
  url?: string,
  data?: {},
}

export function Card({ author_fullname, title, url }: ICardProps) {
  return (
    <li className={styles.card}>
      <div>{title}</div>
      <div><img src={url} alt=""/></div>
      <Dropdown button={<Menu />}>
        <div className={styles.mobileList}>
          <ListItem postId="1234">
          </ListItem>
        </div>
        <div className={styles.desktopList}>
          <ListItemDesktop postId="3322">
          </ListItemDesktop>
        </div>
      
      </Dropdown>
      <Controls />
      <div>{author_fullname}</div>
    </li>
  );
}
