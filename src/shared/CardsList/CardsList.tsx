import React, { useContext } from 'react';
import styles from './cardslist.css';
import { Card } from './Card/Card';
import { postContext} from '../context/PostContext';

export function CardsList() {
  const Postdata = useContext(postContext);

  return (
    <ul className={styles.cardsList}>
      {Postdata.map((item) => {
        return <Card author_fullname={item.data.author_fullname} url={item.data.url} title={item.data.title}/>
      })}
    </ul>
  );
}
