import React from 'react';
import { usePostData } from '../../hooks/usePostData';

interface IPostContextData {
    data: {
        title?: string,
        author_fullname?: string,
        url?: string,
    },
}


export const postContext = React.createContext<IPostContextData[]>([]);

export const PostContextProvider: React.FC = ({ children }) => {
    const [data] = usePostData();

    return (
        <postContext.Provider value={data}>
            { children }
        </postContext.Provider>
    )
}